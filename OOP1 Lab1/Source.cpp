/// <summary>
/// Adding basic and needed libraries
/// </summary>
#include <iostream>
#include <fstream>
#include <string>
#include <ctype.h>

using namespace std;


/// <summary>
/// This function is reading a file to take some ints and number of the ints. 
/// <param name="*ptr">This parameter is a pointer that point to created list in main and save the int numbers</param>
/// <param name="&count">This parameter is reference to count that being in main</param>
/// </summary>
void ReadIntFromTxt(int *ptr, int& count);

/// <summary>
/// This function gives us sum of the integers. 
/// <param name="*ptr">This parameter is a pointer that point to created list in main and save the int numbers</param>
/// <param name="&ref">This parameter is reference to sum that being in main</param>
/// <param name="&count">This parameter is reference to count that being in main</param>
/// </summary>
void SumInt(const int *ptr, int& ref, const int &count);

/// <summary>
/// This function gives us multiplication of numbers. 
/// <param name="*ptr">This parameter is a pointer that point to created list in main and save the int numbers</param>
/// <param name="&ref">This parameter is reference to multip that being in main</param>
/// <param name="&count">This parameter is reference to count that being in main</param>
/// </summary>
void MultipInt(const int *ptr, int& ref, const int& count);

/// <summary>
/// This function gives us smallest of numbers. 
/// <param name="*ptr">This parameter is a pointer that point to created list in main and save the int numbers</param>
/// <param name="&ref">This parameter is reference to smallest that being in main</param>
/// <param name="&count">This parameter is reference to count that being in main</param>
/// </summary>
void SmallestInt(const int *ptr, int& ref, const int& count);


int main()
{
	/// <summary>
	/// <param name="*nums">Array for integers</param>
	/// <param name="count">Count of readed integers</param>
	/// <param name="smallest">Smallest</param>
	/// <param name="sum">Sum</param>
	/// <param name="multip">Multip</param>
	/// </summary>
	int *nums = new int[15];
	int count;
	int smallest, sum = 0, multip = 1;

	/// <summary>
	///Calling the functions
	/// </summary>
	ReadIntFromTxt(nums, count);
	SumInt(nums, sum, count);
	MultipInt(nums, multip, count);
	SmallestInt(nums, smallest, count);

	/// <summary>
	///Printing to the screen
	/// </summary>
	cout << "Sum is: " << sum << endl;
	cout << "Product is: " << multip << endl;
	cout << "Smallest is: " << smallest << endl;
	cin >> *nums;
	return 0;

}
void ReadIntFromTxt(int *ptr, int& count)
{
	string line;
	ifstream file("input.txt");
	count = 0;
	if (file.is_open())
	{
		while (getline(file, line))
		{
			string temp = "";
			for (int i = 0; i < line.length(); i++)
			{
				if (isdigit(line[i]))
				{
					temp += line[i];
				}
				if (((line.length() == i + 1) || !(isdigit(line[i])))&&temp!="")
				{
					ptr[count] = stoi(temp);
					count++;
					temp = "";
				}
			}
		}
	}
}

void SumInt(const int *ptr, int& ref, const int& count)
{
	for (int i = 0; i < count; i++)
	{
		ref += ptr[i];
	}
}

void MultipInt(const int *ptr, int& ref, const int& count)
{
	for (int i = 0; i < count; i++)
	{
		ref *= ptr[i];
	}
}

void SmallestInt(const int *ptr, int& ref, const int& count)
{
	ref = ptr[0];
	for (int i = 1; i < count; i++)
	{
		if (ref > ptr[i])
			ref = ptr[i];
	}
}
