#include "Matrix.h"
#include <iostream>
#include <iomanip>
using namespace std;
/// <summary>
///	This function create dynamic memory for the matrix. 
///	So, "matrix.data" is a pointer to keep pointer address and we had to create array of pointer for this pointer.
/// Then create the memories for these pointers. Also, save the row and column size in matrix.
///	<param name = "matrix">This parameter refers a matrix which created from "struct Matrix" on "Matrix.h".</param>
/// <param name = "rowsize">This parameter is holds a number for matrix rowSize as a integer.</param>
/// <param name = "columnSize">This parameter is holds a number for matrix columnSize as a integer.</param>
/// </summary>
void Matrix_Allocate(Matrix& matrix, int rowSize, int columnSize) {
	matrix.rowSize = rowSize;
	matrix.columnSize= columnSize;

	matrix.data = new float*[rowSize];
	for (int i = 0; i < rowSize; ++i)
		matrix.data[i] = new float[columnSize];
}
/// <summary>
///	This function delete the all memories in matrix.
///	<param name = "matrix">This parameter refers a matrix which created from "struct Matrix" on "Matrix.h".</param>
/// </summary>
void Matrix_Free(Matrix& matrix) {
	for (int i = 0; i < matrix.rowSize; ++i)
		delete[] matrix.data[i];
	delete[] matrix.data;
	matrix.rowSize = -1;
	matrix.columnSize = -1;
}
/// <summary>
///	This function fill the matrixs cell with value.
///	<param name = "matrix">This parameter refers a matrix which created from "struct Matrix" on "Matrix.h".</param>
/// <param name = "value">This parameter is holds a number to fill in the matrix as a float.</param>
/// </summary>
void Matrix_FillByValue(Matrix& matrix, float value) {
	for (int i = 0; i < matrix.rowSize; i++)
	{
		for (int j = 0; j < matrix.columnSize; j++)
		{
			matrix.data[i][j] = value;
		}
	}
}
/// <summary>
/// This function fill the matrixs cell with data.
///	<param name = "matrix">This parameter refers a matrix which created from "struct Matrix" on "Matrix.h".</param>
/// <param name = "**data">This parameter refers to data that is like a matrix as a float.</param>
/// </summary>
void Matrix_FillByData(Matrix & matrix, float ** data) {
	for (int i = 0; i < matrix.rowSize; i++)
	{
		for (int j = 0; j < matrix.columnSize; j++)
		{
			matrix.data[i][j] = data[i][j];
		}
	}
}
/// <summary>
///	This function prints the matrix to console.
///	<param name = "matrix">This parameter refers a matrix which created from "struct Matrix" on "Matrix.h".</param>
/// </summary>
void Matrix_Display(const Matrix& matrix) {
	cout << "MATRIX: " << matrix.rowSize << " X " << matrix.columnSize << endl;
	for (int i = 0; i < matrix.rowSize; i++)
	{
		for (int j = 0; j < matrix.columnSize; j++)
		{
			cout << setw(11)<< matrix.data[i][j];
		}
		cout << endl;
	}
}
/// <summary>
///	This function sum the two matrixs and set the result to new matris.
///	<param name = "matrix_left">This parameter refers a matrix which created from "struct Matrix" on "Matrix.h".</param>
/// <param name = "matrix_right">This parameter refers a matrix which created from "struct Matrix" on "Matrix.h".</param>
/// <param name = "result">This parameter refers a matrix which created from "struct Matrix" on "Matrix.h". But this one is empty.</param>
/// </summary>
void Matrix_Addition(const Matrix & matrix_left, const Matrix & matrix_right,
	Matrix & result) {
	Matrix_Allocate(result,matrix_left.rowSize,matrix_left.columnSize);
	for (int i = 0; i < result.rowSize; i++)
	{
		for (int j = 0; j < result.columnSize; j++)
		{
			result.data[i][j] = matrix_left.data[i][j] + matrix_right.data[i][j];
		}
	}

}
/// <summary>
///	This function sub the two matrixs and set the result to new matris.
///	<param name = "matrix_left">This parameter refers a matrix which created from "struct Matrix" on "Matrix.h".</param>
/// <param name = "matrix_right">This parameter refers a matrix which created from "struct Matrix" on "Matrix.h".</param>
/// <param name = "result">This parameter refers a matrix which created from "struct Matrix" on "Matrix.h". But this one is empty.</param>
/// </summary>
void Matrix_Substruction(const Matrix & matrix_left,
	const Matrix & matrix_right, Matrix & result) {
	Matrix_Allocate(result, matrix_left.rowSize, matrix_left.columnSize);
	for (int i = 0; i < result.rowSize; i++)
	{
		for (int j = 0; j < result.columnSize; j++)
		{
			result.data[i][j] = matrix_left.data[i][j] - matrix_right.data[i][j];
		}
	}
}
/// <summary>
///	This function product the two matrixs and set the result to new matris.
///	<param name = "matrix_left">This parameter refers a matrix which created from "struct Matrix" on "Matrix.h".</param>
/// <param name = "matrix_right">This parameter refers a matrix which created from "struct Matrix" on "Matrix.h".</param>
/// <param name = "result">This parameter refers a matrix which created from "struct Matrix" on "Matrix.h". But this one is empty.</param>
/// </summary>
void Matrix_Multiplication(const Matrix & matrix_left,
	const Matrix & matrix_right, Matrix & result) {
	Matrix_Allocate(result, matrix_left.rowSize, matrix_right.columnSize);

	for (int i = 0; i < result.rowSize; i++)
	{
		for (int j = 0; j < result.columnSize; j++)
		{
			result.data[i][j] = 0;
			for (int z = 0; z < matrix_left.columnSize; z++)
			{
				result.data[i][j] += matrix_left.data[i][z] * matrix_right.data[z][j];
			}
		}
	}
}
/// <summary>
///	This function product the one matrix and scalar value and set the result to new matris.
///	<param name = "matrix_left">This parameter refers a matrix which created from "struct Matrix" on "Matrix.h".</param>
/// <param name = "scalarValue">This parameter hols a number as a float.</param>
/// <param name = "result">This parameter refers a matrix which created from "struct Matrix" on "Matrix.h". But this one is empty.</param>
/// </summary>
void Matrix_Multiplication(const Matrix & matrix_left, float scalarValue,
	Matrix & result) {
	Matrix_Allocate(result, matrix_left.rowSize, matrix_left.columnSize);
	for (int i = 0; i < result.rowSize; i++)
	{
		for (int j = 0; j < result.columnSize; j++)
		{
			result.data[i][j] = matrix_left.data[i][j] * scalarValue;
		}
	}
}
/// <summary>
///	This function divide the one matrix and scalar value and set the result to new matris.
///	<param name = "matrix_left">This parameter refers a matrix which created from "struct Matrix" on "Matrix.h".</param>
/// <param name = "scalarValue">This parameter hols a number as a float.</param>
/// <param name = "result">This parameter refers a matrix which created from "struct Matrix" on "Matrix.h". But this one is empty.</param>
/// </summary>
void Matrix_Division(const Matrix & matrix_left, float scalarValue,
	Matrix & result) {
	Matrix_Allocate(result, matrix_left.rowSize, matrix_left.columnSize);
	for (int i = 0; i < result.rowSize; i++)
	{
		for (int j = 0; j < result.columnSize; j++)
		{
			result.data[i][j] = matrix_left.data[i][j] / scalarValue;
		}
	}
}

void Matrix_Transpose(const Matrix& matrix_left, Matrix& result)
{
	Matrix_Allocate(result, matrix_left.columnSize, matrix_left.rowSize);
	for (int i = 0; i < result.rowSize; i++)
	{
		for (int j = 0; j < result.columnSize; j++)
		{
			result.data[i][j] = matrix_left.data[j][i];
		}
	}

}