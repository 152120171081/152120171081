#include<bits/stdc++.h>

using namespace std;
class Box
{
	friend std::ostream & operator<<(ostream &os, const Box& B);
private:
	int Length, Breadth, Height;
public:
	Box() :Length(0), Breadth(0), Height(0) {};
	Box(int a, int b, int c) :Length(a), Breadth(b), Height(c) {};
	Box(const Box &B)
	{
		Length = B.getLength();
		Breadth = B.getBreadth();
		Height = B.getHeight();
	}
	int getLength()const { return Length; };
	int getBreadth()const { return Breadth; };
	int getHeight()const { return Height; };
	long long CalculateVolume()const
	{
		return long(long(Length*Breadth*Height));
	}
	bool operator<(Box& b)const
	{
		if (Length < b.getLength())
			return true;
		else if (Length > b.getLength())
			return false;
		else
		{
			if (Breadth < b.getBreadth())
				return true;
			else if (Breadth > b.getBreadth())
				return false;
			else
			{
				if (Height < b.getHeight())
					return true;
				else if (Height > b.getHeight())
					return false;
			}
		}
		return false;
	}


};
ostream& operator<<(ostream& out, Box& B)
{
	out << B.getLength() << " " << B.getBreadth() << " " << B.getHeight();
	return out;
}
