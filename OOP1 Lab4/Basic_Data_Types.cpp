#include <iostream>
#include <cstdio>
#include <iomanip>
using namespace std;

int main() {
	int first;
	long second;
	char third;
	float fourth;
	double fifth;
	cin >> first;
	cin >> second;
	cin >> third;
	cin >> fourth;
	cin >> fifth;
	cout << setprecision(16) << first << endl;
	cout << setprecision(16) << second << endl;
	cout << setprecision(16) << third << endl;
	cout << setprecision(16) << fourth << endl;
	cout << setprecision(16) << fifth << endl;
	return 0;
}

