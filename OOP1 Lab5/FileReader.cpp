#include "FileReader.h"


FileReader::FileReader(string filePath)
{
	file.open(filePath);
}


FileReader::~FileReader()
{
	if (file.is_open())
		file.close();
}

void FileReader::read(dataInfo &test)
{
	if (file.is_open())
	{

		string line;
		int CountOfNumbers = NULL;
		getline(file, line);

		if (CountOfNumbers == NULL)
			CountOfNumbers = stoi(line);

		test.countOfNumbers = CountOfNumbers;
		test.data = new char*[CountOfNumbers];

		for (int i = 0; i < CountOfNumbers; i++)
		{
			getline(file, line);
			int digitCount = stoi(line);
			
			test.data[i] = new char[digitCount+2];
			test.data[i][0] = digitCount ;

			getline(file, line);
			for (int j = 1; j < digitCount+1; j++)
			{
				test.data[i][j]=line[j-1];
			}			
			test.data[i][digitCount + 1] = '\0';
		}
		string temp = "";
		while (getline(file, line))
		{
			temp += line;
			test.operationsCount++;
		}
		test.operations = new char*[test.operationsCount];
		for (int i = 0; i < test.operationsCount; i++)
		{			
			test.operations[i] = new char[4];
			int j1 = 0;
			for (int j = 0; j < (temp.size()/test.operationsCount); j++)
			{
				if (temp[(i * 5) + j] != ' ')
				{
					test.operations[i][j1++]= temp[(i * 5) + j];
				}
			}
			test.operations[i][3] = '\0';
		}
		

	}
	else
		cout << "There is a problem while reading." << endl;
}


