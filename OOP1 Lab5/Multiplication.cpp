#include "Multiplication.h"


Multiplication::Multiplication(char x[], char y[])
{
	this->x = x;
	this->y = y;
}

void Multiplication::doMultiplication()
{
	int sizeX = x[0];
	int sizeY = y[0];
	int hand = 0;
	char **tempResult;
	string tempMainX = x;
	string tempMainY = y;
	int tempResultCount = 1;
	int counter = 0;

	int tempX, tempY, tempR;
	if (sizeY < sizeX)
	{
		tempResultCount = sizeY;
		tempResult = new char*[tempResultCount];
		for (int i = 0; i < sizeY; i++)
		{
			tempResult[i] = new char[sizeY + sizeX];
			for (int j = 0; j < (sizeY + sizeX); j++)
			{
				tempResult[i][j] = '-';
			}
		}
		for (int i = sizeY; i > 0; i--)
		{
			int k = sizeX + sizeY - 1;
			for (int p = ((sizeY + sizeX) - counter); p < (sizeY + sizeX); p++)
			{
				tempResult[i - 1][p] = '0';
				k--;
			}
			for (int j = sizeX; j > 0; j--)
			{
				tempX = (tempMainX[j] - 48);
				tempY = (tempMainY[i] - 48);
				tempR = ((tempY * tempX) + hand);
				if (hand != 0 && j == 1)
				{
					hand = tempR / 10;
					tempResult[i - 1][k] = (tempR % 10) + 48;
					tempResult[i - 1][k - 1] = hand + 48;
				}
				else
				{
					hand = tempR / 10;
					tempResult[i - 1][k] = (tempR % 10) + 48;
				}
				k--;
			}
			for (int k = 0; k < sizeY - 1; k++)
			{
				if (i != 1)
					tempResult[i - 1][k] = '0';
			}counter++;
			hand = 0;
		}
	}
	else
	{
		tempResultCount = sizeX;
		tempResult = new char*[tempResultCount];
		for (int i = 0; i < sizeX; i++)
		{
			tempResult[i] = new char[sizeY + sizeX];
			for (int j = 0; j < (sizeY + sizeX); j++)
			{
				tempResult[i][j] = i + 48;
			}
		}
		for (int i = sizeX; i > 0; i--)
		{
			for (int k = ((sizeY + sizeX) - counter); k < (sizeY + sizeX); k++)
				tempResult[i - 1][k] = '0';
			for (int j = sizeY; j > 0; j--)
			{
				tempX = (tempMainX[i] - 48);
				tempY = (tempMainY[j] - 48);
				tempR = ((tempY * tempX) + hand);
				if (hand != 0 && j == 1)
				{
					tempResult[i - 1][j - 1] = (tempR + 48);
				}
				else
				{
					hand = tempR / 10;
					tempResult[i - 1][j - 1] = ((tempR % 10) + 48);
				}
			}

			for (int k = 0; k < sizeX - 1; k++)
				tempResult[i - 1][k] = '0';
			counter++;
		}
	}

	hand = 0;
	int resultSize = sizeX + sizeY + 1;
	result = new char[resultSize];
	string resultTempt = "";
	for (int i = 0; i < resultSize - 1; i++)
	{
		resultTempt += '0';
	}

	for (int k = 0; k < tempResultCount; k++)
	{
		int temp = 0;
		for (int j = (sizeY + sizeX - 1); j > -1; j--)
		{
			temp = (resultTempt[j] - 48);
			if (tempResult[k][j] == '-')
				tempResult[k][j] = '0';
			resultTempt[j] = ((((tempResult[k][j] - 48) + (resultTempt[j] - 48)) + hand) % 10) + 48;
			hand = (((tempResult[k][j] - 48) + temp) / 10);
		}
	}

	for (int i = 0; i < resultSize; i++)
	{
		result[i] = resultTempt[i];
		if (!(result[i] >= '0'&& result[i] <= '9'))
		{
			if (hand == 0)
				result[i] = '\0';
			else
			{
				result[i] = (hand + 48);
				hand = 0;
			}
		}
	}
}

void Multiplication::print()
{
	cout << "The result of ";
	for (int i = 1; i < (x[0]) + 1; i++)
	{
		cout << x[i];
	}
	cout << " x ";
	for (int i = 1; i < (y[0]) + 1; i++)
	{
		cout << y[i];
	}

	cout << " is ";
	int i = 0;
	while (result[i] != '\0')
	{
		if (result[i] == '0'&&i == 0)
		{
			i++;
			continue;
		}
		cout << result[i];
		i++;
	}
	cout << endl;
}
