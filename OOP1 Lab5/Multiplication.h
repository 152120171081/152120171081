#pragma once
#include <iostream>
#include <ctype.h>
#include <sstream>

using namespace std;

#ifndef MULTIPLICATION_H
#define MULTIPLICATION_H

/// <summary> Main class for multiplication. </summary>
/// <param name="*x">This parameter keeps number 1 to use in operation.</param>
/// <param name="*y">This parameter keeps number 2 to use in operation.</param>
/// <param name="*result">This parameter keeps result of (x x y).</param>
class Multiplication
{
	char *x, *y, *result;
public:

	/// <summary>Create an object and set the x and y form main.</summary>
	/// <param name="x[]">This parameter keeps number 1 to use in operation from main.</param>
	/// <param name="y[]">This parameter keeps number 2 to use in operation from main.</param>
	Multiplication(char x[], char y[]);

	/// <summary>This function find the result of (x x y)</summary>
	void doMultiplication();

	/// <summary>Print the screen as (The result of x x y is result)</summary>
	void print();
};

#endif // !1