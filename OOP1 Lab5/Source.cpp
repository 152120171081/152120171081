#include <iostream>
#include <string>
#include <list>
#include "Adition.h"
#include "FileReader.h"
#include "Multiplication.h"
#include "Subtraction.h"

using namespace std;
/// <summary> This function read a file to take nums and operations.</summary>
/// <param name="&Data">This parameter created "struct dataInfo" from "FileReader.h" to keeps the data from file "input.txt"</param>
void readFile(dataInfo &Data);

int main()
{
	dataInfo Data;
	readFile(Data);
	for (int i = 0; i < Data.operationsCount; i++)
	{
		int Num1 = Data.operations[i][1] - 49;
		int Num2 = Data.operations[i][2] - 49;
		if (toupper(Data.operations[i][0]) == 'M')
		{
			Multiplication m(Data.data[Num1], Data.data[Num2]);
			m.doMultiplication();
			m.print();
		}
		else if (toupper(Data.operations[i][0]) == 'A')
		{
			Adition a(Data.data[Num1], Data.data[Num2]);
			a.doAdition();
			a.print();
		}
		else if (toupper(Data.operations[i][0]) == 'S')
		{
			Subtraction s(Data.data[Num1], Data.data[Num2]);
			s.doSubraction();
			s.print();
		}
		else
			cout << "Wrong operation character: " << toupper(Data.operations[i][0]) << endl;
	}	
	return 0;
}


void readFile(struct dataInfo &Data)
{
	FileReader a("input.txt");
	a.read(Data);

}