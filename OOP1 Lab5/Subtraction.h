#pragma once
#include <iostream>
#include <ctype.h>
#include <sstream>
using namespace std;


#ifndef SUBTRACTION_H
#define SUBTRACTION_H

/// <summary >Main class for subtraction. </summary>
/// <param name="*x">This parameter keeps number 1 to use in operation.</param>
/// <param name="*y">This parameter keeps number 2 to use in operation.</param>
/// <param name="*result">This parameter keeps result of (x - y).</param>
class Subtraction
{
	char *x, *y, *result;
public:

	/// <summary>Create an object and set the x and y form main.</summary>
	/// <param name="x[]">This parameter keeps number 1 to use in operation from main.</param>
	/// <param name="y[]">This parameter keeps number 2 to use in operation from main.</param>
	Subtraction(char x[], char y[]);

	/// <summary>This function find the result of (x - y)</summary>
	void doSubraction();

	/// <summary>Print the screen as (The result of x - y is result)</summary>
	void print();
};

#endif