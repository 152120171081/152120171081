#include "Adition.h"


Adition::Adition(char x[], char y[])
{
	this->x = x;
	this->y = y;
}

void Adition::doAdition()
{
	int sizeX = x[0];
	int sizeY = y[0];
	int hand = 0;
	string tempResult = "";
	string tempMainX = x;
	string tempMainY = y;

	while (sizeX > 0 || sizeY > 0)
	{
		stringstream ss;
		int tempX, tempY, tempR;
		if (sizeY > sizeX)
		{
			if (sizeX >= 1)
			{
				tempX = (tempMainX[sizeX] - 48);
				tempY = (tempMainY[sizeY] - 48);
				tempR = (tempY + tempX + hand);
				hand = tempR / 10;
				ss << (tempR % 10);
				tempResult = ss.str() + tempResult;
			}
			if (sizeX < 1)
			{
				tempY = (tempMainY[sizeY] - 48);
				ss << (tempY + hand);
				hand = 0;
				tempResult = ss.str() + tempResult;
			}
		}
		else
		{
			if (sizeY >= 1)
			{
				tempX = (tempMainX[sizeX] - 48);
				tempY = (tempMainY[sizeY] - 48);
				tempR = (tempY + tempX + hand);
				hand = tempR / 10;
				ss << (tempR % 10);
				tempResult = ss.str() + tempResult;
			}
			if (sizeY < 1)
			{
				tempX = (tempMainX[sizeX] - 48);
				ss << (tempX + hand);
				hand = 0;
				tempResult = ss.str() + tempResult;
			}
		}
		sizeX--;
		sizeY--;
	}
	result = new char[tempResult.size() + 1];
	for (int i = 0; i < tempResult.size(); i++)
	{
		result[i] = tempResult[i];
	}
	result[tempResult.size()] = '\0';
}

void Adition::print()
{
	cout << "The result of ";
	for (int i = 1; i < (x[0]) + 1; i++)
	{
		cout << x[i];
	}
	cout << " + ";
	for (int i = 1; i < (y[0]) + 1; i++)
	{
		cout << y[i];
	}

	cout << " is ";
	int i = 0;
	while (result[i] != '\0')
	{
		if (result[i] == '0'&&i == 0)
		{
			i++;
			continue;
		}
		cout << result[i];
		i++;
	}
	cout << endl;
}