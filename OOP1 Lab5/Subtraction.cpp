#include "Subtraction.h"


Subtraction::Subtraction(char x[], char y[])
{
	this->x = x;
	this->y = y;
}

void Subtraction::doSubraction()
{
	int sizeX = x[0];
	int sizeY = y[0];
	int hand = 0;
	string tempResult = "";
	string tempMainX = x;
	string tempMainY = y;

	while (sizeX > 0 || sizeY > 0)
	{
		stringstream ss;
		int tempX, tempY, tempR;
		if (sizeY > sizeX)
		{
			if (sizeX >= 1 && tempMainY[sizeY] < tempMainX[sizeX])
			{
				tempX = (tempMainX[sizeX] - 48);
				tempY = (tempMainY[sizeY] - 48) + 10;
				tempR = (tempY - tempX);
				ss << tempR;
				tempResult = ss.str() + tempResult;
				tempMainY[sizeY - 1] = ((tempMainY[sizeY - 1] - 48) - 1) + 48;
			}
			else if (sizeX >= 1 && tempMainY[sizeY] > tempMainX[sizeX])
			{
				tempX = tempMainX[sizeX] - 48;
				tempY = (tempMainY[sizeY] - 48);
				tempR = (tempY - tempX);
				ss << tempR;
				tempResult = ss.str() + tempResult;
			}
			else if (sizeX >= 1)
			{
				tempX = tempMainX[sizeX] - 48;
				tempY = (tempMainY[sizeY] - 48);
				tempR = (tempY - tempX);
				ss << tempR;
				tempResult = ss.str() + tempResult;

			}
			if (sizeX < 1)
			{
				tempY = (tempMainY[sizeY] - 48);
				ss << tempY;
				tempResult = ss.str() + tempResult;
			}
			if (sizeY == 1)
				tempResult = "-" + tempResult;
			sizeX--;
			sizeY--;
		}
		else
		{
			if (sizeY >= 1 && tempMainX[sizeX] < tempMainY[sizeY])
			{
				tempX = (tempMainX[sizeX] - 48) + 10;
				tempY = (tempMainY[sizeY] - 48);
				tempR = (tempX - tempY);
				ss << tempR;
				tempResult = ss.str() + tempResult;
				tempMainX[sizeX - 1] = ((tempMainX[sizeX - 1] - 48) - 1) + 48;
			}
			else if (sizeY >= 1 && tempMainX[sizeX] > tempMainY[sizeY])
			{
				tempX = tempMainX[sizeX] - 48;
				tempY = tempMainY[sizeY] - 48;
				tempR = (tempX - tempY);
				ss << tempR;
				tempResult = ss.str() + tempResult;
			}
			else if (sizeY >= 1)
			{
				tempR = 0;
				ss << tempR;
				tempResult = ss.str() + tempResult;
			}

			if (sizeY < 1)
			{
				tempX = (tempMainX[sizeX] - 48);
				ss << tempX;
				tempResult = ss.str() + tempResult;
			}
			sizeX--;
			sizeY--;
		}
	}
	result = new char[tempResult.size() + 1];
	for (int i = 0; i < tempResult.size(); i++)
	{
		result[i] = tempResult[i];
	}
	result[tempResult.size()] = '\0';
}

void Subtraction::print()
{
	cout << "The result of ";
	for (int i = 1; i < (x[0]) + 1; i++)
	{
		cout << x[i];
	}
	cout << " - ";
	for (int i = 1; i < (y[0]) + 1; i++)
	{
		cout << y[i];
	}

	cout << " is ";
	int i = 0;
	while (result[i] != '\0')
	{
		if (result[i] == '0'&&i == 0)
		{
			i++;
			continue;
		}
		cout << result[i];
		i++;
	}
	cout << endl;
}