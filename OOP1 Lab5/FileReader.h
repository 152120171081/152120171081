#pragma once
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

#ifndef FILEREADER_H
#define FILEREADER_H

/// <summary> This struct uses with FileReader class to keeps the data. </summary>
/// <param name="**data">This parameter keeps char pointers and uses as matrix.</param>
/// <param name="**operations">This parameter keeps char pointers and uses as matrix.</param>
/// <param name="countOfNumbers">This parameter keeps a number for length of data.</param>
/// <param name="operationsCount">This parameter keeps a number for length of operations.</param>
struct dataInfo
{
	char **data = 0;
	char **operations = 0;
	int countOfNumbers;
	int operationsCount = 0;
};

/// <summary> Main class for file reader. </summary>
/// <param name="filePath">This parameter keeps a string for file's location.</param>
/// <param name="file">This parameter created for reading from fsream libraries.</param>
class FileReader
{
	string	filePath;
	fstream file;
public:
	/// <summary>Create an object for file work and set the filepat from main.</summary>
	FileReader(string filePath);

	/// <summary>If file is not closed, this function close it while deleting.</summary>
	~FileReader();

	/// <summary>Read the file where being filePath an set the test.</summary>
	/// <param name="&test">This parameter keeps an addresses created from main.</param>
	void read(dataInfo &test);	
};

#endif